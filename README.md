Splicing efficiency variations in piRNA clusters
================================================

To avoid library normalization issues when analyzing splicing efficiency, we
adopted an approach based on comparisons between splicing efficiency variations
within piRNA clusters and within genes. For a given pair of libraries, the
distribution of splicing efficiency variations (SEV, measurement detailed
below) for genes constitutes a reference with which the distribution of SEV for
piRNA clusters can be compared. If the splicing in piRNA clusters is affected
in one of the libraries of the pair, the SEV distribution for piRNA clusters
should differ from that for genes. Because we compare gene and piRNA cluster
SEV for a same pair of libraries, our approach is robust to library preparation
differences that could make library-depth normalization difficult, and thus
render comparisons between libraries invalid.

Concretely, the SEV values are determined as follows. For each pair of
libraries of interest, and for each item (whether it is a gene or a piRNA
cluster), the SEV is computed based on the number of reads mapping to potential
exon-exon junctions found by TopHat within that item's genomic coordinates. The
number of junction reads reported by TopHat in each library is normalized by
the length of the item, and a pseudo-count of $0.001$ read per kilobase is
added. The SEV for the considered item is the ratio between the resulting
length-normalized read junction counts for the two libraries. The use of a
pseudo-count avoids discarding items for which no splicing is detected in one
of the libraries.

The distribution of the SEV values for piRNA clusters and for genes are
compared using a Wilcoxon rank-sum test (as implemented in the SciPy library).

The results are synthesized in a "volcano plot" where each pair of libraries is
represented by a point. The x coordinate represents the ratio between piRNA
cluster median SEV and gene median SEV. Th y coordinate is the p-value of the
Wilcoxon rank-sum test. The higher the ratio between piRNA cluster and gene
median SEV, the stronger the splicing de-repression in piRNA clusters between
the compared libraries. For graphical clarity, p-values lower than $10^{-6}$
are set to $10^{-6}$.

[//]: # (Alternative representation, using the "symlog" option:)
[//]: # (The y axis is linear between $0$ and $5 \times{} 10^{-6}$, then logarithmic.)
[//]: # (This enables the representation of pairs of libraries for which the p-value of the Wilcoxon rank-sum test has been rounded to zero.)
