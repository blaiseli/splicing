"""

Snakefile to perform quantification of splice junction of genes and piRNA
clusters using tophat.

"""

import os
OPJ = os.path.join
GENOMES = os.environ.get("GENOMES")
genome="D_melanogaster"
clust_num = 142

# Or use --configfile instead
#configfile:
#    "tophat_junction_discovery.config.json"

# key: library name
# value: SRR number
lib2sr = config["lib2sr"]
# key: library name
# value: path to raw data
lib2raw = config["lib2raw"]
LIBS = list(lib2sr.keys()) + list(lib2raw.keys())
#CLUST_NUMS = range(1, clust_num + 1)

raw_data_dir = config["raw_data_dir"]
output_dir = config["output_dir"]
annot_dir = config["annot_dir"]

#CLUST_COUNTS = dict((n, OPJ(output_dir, "done_{{lib}}_cluster{n}.txt")) for n in CLUST_NUMS)
#CLUST_COUNTS = [OPJ(output_dir, "done_{{lib}}_cluster{n}.txt") for n in CLUST_NUMS]
#CLUST_COORDS = [OPJ(annot_dir, "piRNA_cluster{i}.bed") for n in CLUST_NUMS]

rule all:
    input:
        expand(OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts_raw.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts_raw.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts_raw.txt"), lib=LIBS),
        #expand(OPJ(output_dir, "done_{lib}_cluster{i}.txt"), lib=LIBS, i=CLUST_NUMS),
        #expand(OPJ(raw_data_dir, "{lib}_{read}.fastq"), lib=LIBS, read=['1','2']),
        #expand(OPJ(output_dir, "{lib}_junctions.bed"), lib=LIBS),
        #expand(OPJ(annot_dir, "piRNA_cluster{i}.bed"), i=CLUST_NUMS)


rule count_junction_reads_in_clusters:
    input:
        clusters = OPJ(annot_dir, "piRNA_cluster.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        norm = OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts.txt"),
    shell:
        # bedtools intersect options:
        # Perform a "left outer join".
        # That is, for each feature in A report each overlap with B.
        # If no overlaps are found, report a NULL feature for B.
        # -loj
        # NULL features have -1 in the field corresponding to the junction read counts
        # (field 11 in the resulting output).
        # We want junctions that are fully inside the gene:
        # Minimum overlap required as a fraction of B. Default is 1E-9 (i.e., 1bp).
        # -F 1
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.norm}
        bedtools intersect \\
            -a {input.clusters} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += (1000 * $11) / ($3 - $2)}} END {{for (clust in counts) print lib"\\t"clust"\\t"counts[clust]}}' \\
            | sed 's/piRNA_cluster@//' \\
            | sort -n -k2.9 \\
            >> {output.norm}
        """


rule count_raw_junction_reads_in_clusters:
    input:
        clusters = OPJ(annot_dir, "piRNA_cluster.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        raw = OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts_raw.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads" > {output.raw}
        bedtools intersect \\
            -a {input.clusters} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += $11}} END {{for (clust in counts) print lib"\\t"clust"\\t"counts[clust]}}' \\
            | sed 's/piRNA_cluster@//' \\
            | sort -n -k2.9 \\
            >> {output.raw}
        """


rule count_junction_reads_in_genes:
    input:
        genes = OPJ(annot_dir, "genes_outside_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        norm = OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.norm}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += (1000 * $11) / ($3 - $2)}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.norm}
        """


rule count_raw_junction_reads_in_genes:
    input:
        genes = OPJ(annot_dir, "genes_outside_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        raw = OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts_raw.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.raw}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += $11}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.raw}
        """


rule count_junction_reads_in_cluster_genes:
    input:
        genes = OPJ(annot_dir, "genes_overlapping_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        norm = OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.norm}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += (1000 * $11) / ($3 - $2)}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.norm}
        """


rule count_raw_junction_reads_in_cluster_genes:
    input:
        genes = OPJ(annot_dir, "genes_overlapping_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        raw = OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts_raw.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.raw}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += $11}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.raw}
        """


rule find_genes_overlapping_clusters:
    input:
        genes = OPJ(annot_dir, "genes.bed"),
        clusters = OPJ(annot_dir, "piRNA_clusters.bed"),
    output:
        genes_overlapping_clusters = OPJ(annot_dir, "genes_overlapping_clusters.bed"),
    shell:
        """
        # We dont use -f 1.0 here because we only want overlap, not inclusion.
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.clusters} \\
            -u \\
            -wa \\
            > {output.genes_overlapping_clusters}
        """
    

rule find_genes_outside_clusters:
    input:
        genes = OPJ(annot_dir, "genes.bed"),
        clusters = OPJ(annot_dir, "piRNA_clusters.bed"),
        all_tophat = expand(OPJ(output_dir, "{lib}", "accepted_hits.bam"), lib=LIBS),
    output:
        not_cluster = OPJ(annot_dir, "genes_outside_clusters.bed"),
        genes_outside_clusters = OPJ(annot_dir, "genes_outside_clusters.bed"),
    shell:
        """
        for lib in %s
        do
            samtools view -H %s/${{lib}}/accepted_hits.bam \\
                | awk '$1 == "@SQ" {{print $2"\\t"$3}}' \\
                | sed 's/[SL]N://g'
        done | sort -u > %s/chrom_sizes.txt
        bedtools complement \\
            -i {input.clusters} \\
            -g %s/chrom_sizes.txt \\
            > {output.not_cluster}
        bedtools intersect \\
            -a {input.genes} \\
            -b {output.not_cluster} \\
            -f 1.0 \\
            -u \\
            -wa \\
            > {output.genes_outside_clusters}
        """ % (" ".join(LIBS), output_dir, output_dir, output_dir)
    

rule run_tophat:
    input:
        transcriptome = OPJ(annot_dir, "dmel-all-r5.9.gff"),
        #fastq_files = expand("{fq1} {fq2}".format(fq1=OPJ(raw_data_dir, "{lib}_1.fastq"), fq2=OPJ(raw_data_dir, "{lib}_2.fastq")), lib=LIBS)
        fq1 = OPJ(raw_data_dir, "{lib}_1.fastq.gz"),
        fq2 = OPJ(raw_data_dir, "{lib}_2.fastq.gz"),
        #fq1 = expand(OPJ(raw_data_dir, "{lib}_1.fastq"), lib=LIBS),
        #fq2 = expand(OPJ(raw_data_dir, "{lib}_2.fastq"), lib=LIBS)
    params:
        genome_db = OPJ(GENOMES, genome, genome),
        log_dir = OPJ(output_dir, "logs"),
        res_dir = lambda wildcards : OPJ(output_dir, wildcards.lib),
    output:
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
        bam = OPJ(output_dir, "{lib}", "accepted_hits.bam"),
    shell:
        """
        mkdir -p {params.log_dir}
        mkdir -p {params.res_dir}
        time tophat -p $(nproc) -o {params.res_dir} \\
            {params.genome_db} {input.fq1} {input.fq2} \\
            -G {input.transcriptome} \\
            -x 1000 -g 1000 \\
            --read-mismatches 2 \\
            --read-edit-dist 2 \\
            --read-realign-edit-dist 0 \\
            --segment-length 50 \\
            --segment-mismatches 2 \\
            >> {params.log_dir}/{wildcards.lib}.log \\
            2>> {params.log_dir}/{wildcards.lib}.err
        """


def get_sr(wildcards):
    #print("get_raw_data", wildcards.lib)
    return lib2sr[wildcards.lib]


def lib2data(wildcards):
    lib = wildcards.lib
    if lib in lib2sr:
        return "fastq-dump -F --split-3 --gzip -A %s %s" % (lib, lib2sr[lib])
    elif lib in lib2raw:
        raw = lib2raw[lib]
        link_1 = "ln -s %s %s_1.fastq.gz" % (raw.format(mate="1"), lib)
        link_2 = "ln -s %s %s_2.fastq.gz" % (raw.format(mate="2"), lib)
        return "%s\n%s\n" % (link_1, link_2)
    else:
        raise ValueError("Procedure to get raw data for %s unknown." % lib)


rule get_raw_data:
    input:
        raw_data_dir
    output:
        #expand(OPJ(raw_data_dir, "{lib}_1.fastq"), lib=LIBS),
        #expand(OPJ(raw_data_dir, "{lib}_2.fastq"), lib=LIBS)
        OPJ(raw_data_dir, "{lib}_1.fastq.gz"),
        OPJ(raw_data_dir, "{lib}_2.fastq.gz"),
    params:
        shell_command = lib2data,
        #sr = lambda wildcards : lib2sr[wildcards.lib]
    shell:
        """
        (
        cd {input}
        {params.shell_command}
        )
        """
        #"fastq-dump -F --split-3 --gzip -A {wildcards.lib} {params.sr}"
        #"fastq-dump -F --split-3 --gzip -A {wildcards.lib} {params.sr}"


rule get_transcriptome_annotations:
    input:
        annot_dir,
    output:
        OPJ(annot_dir, "dmel-all-r5.9.gff"),
    shell:
        """
        (
        cd {input}
        wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/gff/dmel-all-r5.9.gff.gz
        gunzip dmel-all-r5.9.gff.gz
        )
        """


rule get_miscRNA_annotations:
    input:
        annot_dir,
    output:
        OPJ(annot_dir, "miscRNA.bed"),
    shell:
        """
        (
        cd {input}
        wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/fasta/dmel-all-miscRNA-r5.9.fasta.gz
        gunzip dmel-all-miscRNA-r5.9.fasta.gz
        )
        fbfasta2bed.py {input}/dmel-all-miscRNA-r5.9.fasta \\
            | sort -k1,1 -k2n,2 -k3n,3 \\
            > {output}
        """


rule get_gene_annotations:
    input:
        annot_dir,
    output:
        OPJ(annot_dir, "genes.bed"),
    shell:
        """
        (
        cd {input}
        wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/fasta/dmel-all-gene-r5.9.fasta.gz
        gunzip dmel-all-gene-r5.9.fasta.gz
        )
        fbfasta2bed.py {input}/dmel-all-gene-r5.9.fasta \\
            | sort -k1,1 -k2n,2 -k3n,3 \\
            > {output}
        """


rule get_pi_cluster_annotations:
    input:
        annot_dir = annot_dir,
        bed_file = OPJ(annot_dir, "piRNA_clusters.bed"),
    output:
        OPJ(annot_dir, "piRNA_cluster{i}.bed"),
    shell:
        """
        grep "\\bpiRNA_cluster@cluster${{wildcards.i}}\\b" {input.bed_file} > {output}
        """

rule make_cluster_bed_file:
    input:
        annot_dir,
    output:
        OPJ(annot_dir, "piRNA_clusters.bed"),
    shell:
        """
        grep "piRNA_cluster@" ~/Genomes/D_melanogaster/Annotations/D_melanogaster_flybase_splitmRNA_and_TE_annot.bed \\
            | sed -e 's/^/chr/' \\
            > {output}
        """

rule make_dirs:
    output:
        raw_data_dir,
        OPJ(output_dir, "junction_counts"),
        annot_dir,
    run:
        for dir in output:
            os.makedirs(dir, exist_OK=True)

