"""

Snakefile to perform quantification of splice junction of genes and piRNA
clusters using CRAC.

"""

import os
OPJ = os.path.join
GENOMES = os.environ.get("GENOMES")
genome="D_melanogaster"
clust_num = 142

# Use --configfile instead
#configfile:
#    "tophat_junction_discovery.config.json"

# key: library name
# value: SRR number
lib2sr = config["lib2sr"]
lib2sr_single_end = config["lib2sr_single_end"]
# key: library name
# value: path to raw data
lib2raw = config["lib2raw"]
lib2raw_single_end = config["lib2raw_single_end"]
LIBS = list(lib2sr.keys()) + list(lib2raw.keys()) + list(lib2raw_single_end.keys()) + list(lib2sr_single_end.keys())
#CLUST_NUMS = range(1, clust_num + 1)

raw_data_dir = config["raw_data_dir"]
output_dir = config["output_dir"]
annot_dir = config["annot_dir"]

#CLUST_COUNTS = dict((n, OPJ(output_dir, "done_{{lib}}_cluster{n}.txt")) for n in CLUST_NUMS)
#CLUST_COUNTS = [OPJ(output_dir, "done_{{lib}}_cluster{n}.txt") for n in CLUST_NUMS]
#CLUST_COORDS = [OPJ(annot_dir, "piRNA_cluster{i}.bed") for n in CLUST_NUMS]


rule all:
    input:
        expand(OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts_raw.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts_raw.txt"), lib=LIBS),
        expand(OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts_raw.txt"), lib=LIBS),
        #expand(OPJ(output_dir, "{lib}_junctions.bed"), lib=LIBS),
        expand(OPJ(output_dir, "{lib}", "pysam_junctions.bed"), lib=LIBS),
        expand(OPJ(output_dir, "{lib}", "crac_sorted.bam.bai"), lib=LIBS),
        expand(OPJ(output_dir, "{lib}", "crac_sorted.bam"), lib=LIBS),


####################
# Annotation stuff #
####################

rule get_transcriptome_annotations:
    #input:
    #    annot_dir,
    output:
        OPJ(annot_dir, "dmel-all-r5.9.gff"),
    shell:
        """
        wget -O - ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/gff/dmel-all-r5.9.gff.gz | zcat > {output}
        """


rule get_miscRNA_annotations:
    #input:
    #    annot_dir,
    output:
        OPJ(annot_dir, "miscRNA.bed"),
    params:
        annot_dir = annot_dir
    shell:
        """
        (
        cd {annot_dir}
        wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/fasta/dmel-all-miscRNA-r5.9.fasta.gz
        gunzip dmel-all-miscRNA-r5.9.fasta.gz
        )
        fbfasta2bed.py {annot_dir}/dmel-all-miscRNA-r5.9.fasta \\
            | sort -k1,1 -k2n,2 -k3n,3 \\
            > {output}
        """


rule get_gene_annotations:
    #input:
    #    annot_dir,
    output:
        OPJ(annot_dir, "genes.bed"),
    params:
        annot_dir = annot_dir
    shell:
        """
        (
        cd {annot_dir}
        wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/fasta/dmel-all-gene-r5.9.fasta.gz
        gunzip dmel-all-gene-r5.9.fasta.gz
        )
        fbfasta2bed.py {annot_dir}/dmel-all-gene-r5.9.fasta \\
            | sort -k1,1 -k2n,2 -k3n,3 \\
            > {output}
        """


rule get_pi_cluster_annotations:
    input:
        #annot_dir = annot_dir,
        bed_file = OPJ(annot_dir, "piRNA_clusters.bed"),
    output:
        OPJ(annot_dir, "piRNA_cluster{i}.bed"),
    shell:
        """
        grep "\\bpiRNA_cluster@cluster${{wildcards.i}}\\b" {input.bed_file} > {output}
        """

rule make_cluster_bed_file:
    #input:
    #    annot_dir,
    output:
        OPJ(annot_dir, "piRNA_clusters.bed"),
    shell:
        """
        grep "piRNA_cluster@" ~/Genomes/D_melanogaster/Annotations/D_melanogaster_flybase_splitmRNA_and_TE_annot.bed \\
            | sed -e 's/^/chr/' \\
            > {output}
        """


####################
# Input data stuff #
####################

def get_sr(wildcards):
    #print("get_raw_data", wildcards.lib)
    return lib2sr[wildcards.lib]


def lib2data(wildcards):
    lib = wildcards.lib
    if lib in lib2sr:
        return "fastq-dump -F --split-3 --gzip -A %s %s" % (lib, lib2sr[lib])
    elif lib in lib2sr_single_end:
        return "fastq-dump -F --gzip -A %s %s" % (lib, lib2sr[lib])
    elif lib in lib2raw:
        raw = lib2raw[lib]
        link_1 = "ln -s %s %s_1.fastq.gz" % (raw.format(mate="1"), lib)
        link_2 = "ln -s %s %s_2.fastq.gz" % (raw.format(mate="2"), lib)
        return "%s\n%s\n" % (link_1, link_2)
    elif lib in lib2raw_single_end:
        raw = lib2raw_single_end[lib]
        return "ln -s %s %s.fastq.gz\n" % (raw, lib)
    else:
        raise ValueError("Procedure to get raw data for %s unknown." % lib)


def lib2data_input(wildcards):
    lib = wildcards.lib
    if lib in lib2sr:
        return []
    elif lib in lib2sr_single_end:
        return []
    elif lib in lib2raw:
        raw = lib2raw[lib]
        return [raw.format(mate="1"), raw.format(mate="2")]
    elif lib in lib2raw_single_end:
        raw = lib2raw_single_end[lib]
        return [raw]
    else:
        raise ValueError("Procedure to get raw data for %s unknown." % lib)

#def raw_data_input(wildcards):
#    lib = wildcards.lib
#    if lib in lib2sr:
#        return []
#    elif lib in lib2raw:
#        raw = lib2raw[lib]
#        return [raw.format(mate="1"), raw.format(mate="2")]
#    else:
#        raise ValueError("Procedure to get raw data for %s unknown." % lib)

rule get_raw_data:
    input:
        lib2data_input
    output:
        #expand(OPJ(raw_data_dir, "{lib}_1.fastq"), lib=LIBS),
        #expand(OPJ(raw_data_dir, "{lib}_2.fastq"), lib=LIBS)
        OPJ(raw_data_dir, "{lib}_1.fastq.gz"),
        OPJ(raw_data_dir, "{lib}_2.fastq.gz"),
    params:
        shell_command = lib2data,
        #sr = lambda wildcards : lib2sr[wildcards.lib]
    shell:
        """
        (
        cd {raw_data_dir}
        {params.shell_command}
        )
        """
        #"fastq-dump -F --split-3 --gzip -A {wildcards.lib} {params.sr}"
        #"fastq-dump -F --split-3 --gzip -A {wildcards.lib} {params.sr}"


#def raw_data_input_single_end(wildcards):
#    lib = wildcards.lib
#    if lib in lib2sr:
#        return []
#    elif lib in lib2raw_single_end:
#        raw = lib2raw_single_end[lib]
#        return [raw]
#    else:
#        raise ValueError("Procedure to get raw data for %s unknown." % lib)

rule get_raw_data_single_end:
    input:
        lib2data_input
    output:
        OPJ(raw_data_dir, "{lib}.fastq.gz"),
    params:
        shell_command = lib2data,
    shell:
        """
        (
        cd {raw_data_dir}
        {params.shell_command}
        )
        """

################
# Running crac #
################

def lib2fq(wildcards):
    lib = wildcards.lib
    if lib in lib2raw_single_end:
        return rules.get_raw_data_single_end.output
    elif lib in lib2sr_single_end:
        return rules.get_raw_data_single_end.output
    else:
        return rules.get_raw_data.output

rule check_read_lengths:
    input:
        fq = lib2fq,
    output:
        OPJ(output_dir, "{lib}", "reads_length.txt")
    shell:
        """
        for read_file in {input.fq}
        do
            case ${{read_file}} in
                *.gz)
                    zcat ${{read_file}} | fastq2histo.awk
                    ;;
                *.bz2)
                    bzcat ${{read_file}} | fastq2histo.awk
                    ;;
                *)
                    fastq2histo.awk ${{read_file}}
                    ;;
            esac
        done \\
            | awk '{{print $1}}' | sort -u > {output}
        if [ $(cat {output} | wc -l) -ne 1 ]
        then
            # empty the file
            > {output}
        fi
        """

rule run_crac:
    input:
        reads_length = OPJ(output_dir, "{lib}", "reads_length.txt"),
        #fastq_files = expand("{fq1} {fq2}".format(fq1=OPJ(raw_data_dir, "{lib}_1.fastq"), fq2=OPJ(raw_data_dir, "{lib}_2.fastq")), lib=LIBS)
        fq = lib2fq,
        #fq = rules.get_raw_data.output,
        #fq1 = OPJ(raw_data_dir, "{lib}_1.fastq.gz"),
        #fq2 = OPJ(raw_data_dir, "{lib}_2.fastq.gz"),
        #fq1 = expand(OPJ(raw_data_dir, "{lib}_1.fastq"), lib=LIBS),
        #fq2 = expand(OPJ(raw_data_dir, "{lib}_2.fastq"), lib=LIBS)
    params:
        genome_db = OPJ(GENOMES, genome, genome),
        log_dir = OPJ(output_dir, "logs"),
        res_dir = lambda wildcards : OPJ(output_dir, wildcards.lib),
    output:
        sam = temp(OPJ(output_dir, "{lib}", "crac.sam")),
    shell:
        """
        rl=$(cat {input.reads_length})
        if [ ${{rl}} ]
        then
            rl_option="-m ${{rl}}"
        else
            rl_option=""
        fi
        mkdir -p {params.log_dir}
        mkdir -p {params.res_dir}
        time crac -i {params.genome_db} \\
            -r {input.fq} \\
            -k 22 ${{rl_option}} \\
            --sam {output} \\
            --nb-threads {threads} \\
            >> {params.log_dir}/{wildcards.lib}.log \\
            2>> {params.log_dir}/{wildcards.lib}.err
        """


rule sam2indexedbam:
    input:
        sam = OPJ(output_dir, "{lib}", "crac.sam"),
    output:
        sorted_bam = protected(OPJ(output_dir, "{lib}", "crac_sorted.bam")),
        index = protected(OPJ(output_dir, "{lib}", "crac_sorted.bam.bai")),
    message:
        "Sorting and indexing sam file for {wildcards.lib}."
    params:
        log_dir = OPJ(output_dir, "logs"),
    shell:
        """
        nice -n 19 ionice -c2 -n7 sam2indexedbam.sh {input.sam} \\
            1> {params.log_dir}/{wildcards.lib}_sam2indexbam.log \\
            2> {params.log_dir}/{wildcards.lib}_sam2indexbam.err
        """


#TODO: Try using pysam find_introns method
import pysam
from itertools import tee
from operator import itemgetter


def junction_alis(samfile, ref):
    for ali in samfile.fetch(reference=ref):
        if not ali.has_tag("XE"):
            continue
        sub_tags = ali.get_tag("XE").split(";")
        for sub_tag in sub_tags:
            tag_parts = sub_tag.split(":")
            if tag_parts[2] != "Junction":
                continue
            if tag_parts[3] == "normal":
                yield (ali, ali.is_reverse)


def crac2junctions(crac_filename):
    """*crac_filename* is the path to a sorted and indexed sam file generated by crac."""
    with pysam.AlignmentFile(crac_filename) as samfile:
        for ref in samfile.references:
            alis_provider, orientation_provider = tee(junction_alis(samfile, ref))
            introns_info = samfile.find_introns(map(itemgetter(0), alis_provider)).items()
            signs_info = map(itemgetter(1), orientation_provider)
            for num, (start, stop, count, is_reverse) in enumerate(sorted(
                (start, stop, count, is_reverse) for ((start, stop), count), is_reverse in zip(
                    introns_info, signs_info))):
            #for num, ((start, stop), count) in enumerate(sorted(samfile.find_introns(
            #    junction_alis(samfile, ref)).items())):
                if is_reverse:
                    yield "\t".join([
                        ref,
                        str(start),
                        str(stop),
                        "Junction:%s:%d" % (ref, num),
                        str(count),
                        "-"])
                else:
                    yield "\t".join([
                        ref,
                        str(start),
                        str(stop),
                        "Junction:%s:%d" % (ref, num),
                        str(count),
                        "+"])

rule bam2juncbed_using_pysam:
    input:
        sorted_bam = OPJ(output_dir, "{lib}", "crac_sorted.bam"),
        index = OPJ(output_dir, "{lib}", "crac_sorted.bam.bai"),
    output:
        junctions = OPJ(output_dir, "{lib}", "pysam_junctions.bed"),
    run:
        with open(output.junctions, "w") as junctions_file:
            junctions_file.write("\n".join(crac2junctions(input.sorted_bam)))


rule bam2juncbed:
    input:
        sorted_bam = OPJ(output_dir, "{lib}", "crac_sorted.bam"),
        index = OPJ(output_dir, "{lib}", "crac_sorted.bam.bai"),
    output:
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    shell:
        """
        cractools extract {input.sorted_bam} --splices {output.junctions}
        """


######################
# Counting junctions #
######################

rule count_junction_reads_in_clusters:
    input:
        clusters = OPJ(annot_dir, "piRNA_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        norm = OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts.txt"),
    shell:
        # bedtools intersect options:
        # Perform a "left outer join".
        # That is, for each feature in A report each overlap with B.
        # If no overlaps are found, report a NULL feature for B.
        # -loj
        # NULL features have -1 in the field corresponding to the junction read counts
        # (field 11 in the resulting output).
        # We want junctions that are fully inside the gene:
        # Minimum overlap required as a fraction of B. Default is 1E-9 (i.e., 1bp).
        # -F 1
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.norm}
        bedtools intersect \\
            -a {input.clusters} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += (1000 * $11) / ($3 - $2)}} END {{for (clust in counts) print lib"\\t"clust"\\t"counts[clust]}}' \\
            | sed 's/piRNA_cluster@//' \\
            | sort -n -k2.9 \\
            >> {output.norm}
        """


rule count_raw_junction_reads_in_clusters:
    input:
        clusters = OPJ(annot_dir, "piRNA_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        raw = OPJ(output_dir, "junction_counts", "{lib}", "piRNA_cluster_junction_counts_raw.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads" > {output.raw}
        bedtools intersect \\
            -a {input.clusters} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += $11}} END {{for (clust in counts) print lib"\\t"clust"\\t"counts[clust]}}' \\
            | sed 's/piRNA_cluster@//' \\
            | sort -n -k2.9 \\
            >> {output.raw}
        """


rule count_junction_reads_in_genes:
    input:
        genes = OPJ(annot_dir, "genes_outside_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        norm = OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.norm}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += (1000 * $11) / ($3 - $2)}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.norm}
        """


rule count_raw_junction_reads_in_genes:
    input:
        genes = OPJ(annot_dir, "genes_outside_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        raw = OPJ(output_dir, "junction_counts", "{lib}", "gene_junction_counts_raw.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.raw}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += $11}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.raw}
        """


rule count_junction_reads_in_cluster_genes:
    input:
        genes = OPJ(annot_dir, "genes_overlapping_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        norm = OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.norm}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += (1000 * $11) / ($3 - $2)}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.norm}
        """


rule count_raw_junction_reads_in_cluster_genes:
    input:
        genes = OPJ(annot_dir, "genes_overlapping_clusters.bed"),
        junctions = OPJ(output_dir, "{lib}", "junctions.bed"),
    output:
        raw = OPJ(output_dir, "junction_counts", "{lib}", "cluster_gene_junction_counts_raw.txt"),
    shell:
        """
        echo -e "#library\\treference\\tjunction_reads_per_kb" > {output.raw}
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.junctions} \\
            -loj \\
            -F 1 \\
            | awk -v lib="{wildcards.lib}" '($11 < 1){{counts[$4] += 0}} ($11 > 0){{counts[$4] += $11}} END {{for (gene in counts) print lib"\\t"gene"\\t"counts[gene]}}' \\
            >> {output.raw}
        """


rule find_genes_overlapping_clusters:
    input:
        genes = OPJ(annot_dir, "genes.bed"),
        clusters = OPJ(annot_dir, "piRNA_clusters.bed"),
    output:
        genes_overlapping_clusters = OPJ(annot_dir, "genes_overlapping_clusters.bed"),
    shell:
        """
        # We dont use -f 1.0 here because we only want overlap, not inclusion.
        bedtools intersect \\
            -a {input.genes} \\
            -b {input.clusters} \\
            -u \\
            -wa \\
            > {output.genes_overlapping_clusters}
        """
    

rule find_genes_outside_clusters:
    input:
        genes = OPJ(annot_dir, "genes.bed"),
        clusters = OPJ(annot_dir, "piRNA_clusters.bed"),
        all_sorted_bam = expand(OPJ(output_dir, "{lib}", "crac_sorted.bam"), lib=LIBS),
    output:
        not_cluster = OPJ(annot_dir, "genes_outside_clusters.bed"),
        genes_outside_clusters = OPJ(annot_dir, "genes_outside_clusters.bed"),
    shell:
        """
        for lib in %s
        do
            samtools view -H %s/${{lib}}/crac_sorted.bam \\
                | awk '$1 == "@SQ" {{print $2"\\t"$3}}' \\
                | sed 's/[SL]N://g'
        done | sort -u > %s/chrom_sizes.txt
        bedtools complement \\
            -i {input.clusters} \\
            -g %s/chrom_sizes.txt \\
            > {output.not_cluster}
        bedtools intersect \\
            -a {input.genes} \\
            -b {output.not_cluster} \\
            -f 1.0 \\
            -u \\
            -wa \\
            > {output.genes_outside_clusters}
        """ % (" ".join(LIBS), output_dir, output_dir, output_dir)
    


