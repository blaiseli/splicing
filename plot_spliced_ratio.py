#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script will read splicing quantification and plot the information."""


import argparse
import sys
WRITE = sys.stdout.write

# To avoid repeatedly calling these as string methods:
from string import split, strip
#from re import sub

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

#from itertools import cycle, imap, izip, repeat
from itertools import imap
#from collections import OrderedDict
from collections import defaultdict
#from operator import itemgetter
from scipy import stats
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
mpl.use("PDF")

#mpl.rcParams["figure.figsize"] = 2, 4
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"

#import pylab
from matplotlib import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(precision=10)


def histo(axis, lib1, lib2):
    """Generates histograms."""
    box_data = []
    box_data.append(Globals.ratios[(lib1, lib2)]["others"])
    axis.hist(box_data, bins=50, align="mid")


def median_fold_and_pvalue(data1, data2):
    """Returns the ratio of the medians of the two data series *data1* and
    *data2* and the pvalue of a Wilcoxon rank-sum test."""
    median_fold = np.median(data1) / np.median(data2)
    _, pvalue = stats.ranksums(data1, data2)
    return median_fold, pvalue

def box_plot(axis, lib1, lib2, ref_types, transform=False):
    """Generates the boxplots for the references not in
    *Globals.exclude_list* using splice / unspliced ratio
    and adds it to the subplot *axis*."""
    if transform:
        axis.set_yscale("log", basey=transform)
        if transform == 2:
            plt.minorticks_off()
    box_data = []
    x_labels = []
    for ref_type in ref_types:
        box_data.append(Globals.ratios[ref_type])
        #x_labels.append("")
        x_labels.append(ref_type.replace("_", " "))
    # Not sure it works to put more than 2 whiskers
    axis.boxplot(box_data, whis=[5, 95])
    #axis.violinplot(box_data, showmedians=True)
    #axis.set_xlim(right=(len(x_labels) + 0.5))
    #axis.set_xticks(range(1, 1 + len(x_labels)))
    #plt.xticks(rotation=70)
    axis.set_ylabel("%s / %s junction reads per kb ratio" % (lib1, lib2))
    axis.set_xticklabels(x_labels)
    #for lib in Globals.lib_order:
    #    missing = missing_data[lib]
    #    msg = "\n%d missing in %s: %s\n" % (
    #        len(missing),
    #        lib,
    #        ", ".join(missing))
    #    warnings.warn(msg)


def get_data(data_file_name, ref_type):
    """Reads data from file *data_file_name* where the file should be consist
    in one header line with library names in the second and third columns
    followed by data lines, with the first column being a genomic region name
    and the two other columns the junction read counts in the two libraries and
    normalized by the region length.
    Records the pairs of values in Globals.pairs[ref_type]
    Returns the names of the two libraries."""
    pairs = Globals.pairs[ref_type]
    exclude_set = Globals.exclude_set
    with open(data_file_name, "r") as data_file:
        [_, lib1, lib2] = split(strip(data_file.readline().lstrip("#")), "\t")

        def extract_fields(line):
            """Selects and appropriately converts the fields we want."""
            [ref, count1, count2] = split(line)
            return ref, float(count1), float(count2)
        if Globals.no_zero_pairs:
            def append_pair(ref, count1, count2):
                """Appends a pair to the list of pairs *pairs*"""
                if (ref not in exclude_set) and (count1 > 0) and (count2 > 0):
                    pairs.append((count1, count2))
        else:
            def append_pair(ref, count1, count2):
                """Appends a pair to the list of pairs *pairs*"""
                if ref not in exclude_set:
                    pairs.append((count1, count2))
        for line in imap(strip, data_file):
            if line[0] == "#":
                continue
            ref, count1, count2 = extract_fields(line)
            append_pair(ref, count1, count2)
        return lib1, lib2


#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################


class Globals(object):
    """This object holds global variables."""
    # To not take into account (0, 0) data pairs.
    no_zero_pairs = True
    # Set of elements to exclude from the boxplot data
    exclude_set = set()
    # key: reference type
    # value: list of pairs of values
    pairs = defaultdict(list)
    # key: reference type
    # value: list of ratios
    ratios = defaultdict(list)


def main():
    """Main function of the program."""
    sys.stderr.write("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-i", "--in_files",
        nargs="*",
        required=True,
        help="Space-separated list of data files.")
    parser.add_argument(
        "-r", "--ref_types",
        nargs="*",
        required=True,
        help="Space-separated list of reference types.\n"
        "These are names to associate to the files "
        "provided with option --in_files.\n"
        "If two reference types are provided, the program will output "
        "the coordinates of a volcano-plot point using as x-value the ratio "
        "of the median of the ratios for the first reference type to "
        "the median of the ratios for the second, and as y-value "
        "the p-value of the Wolcoxon ran-sum test comparing "
        "the two ratio distributions.")
    parser.add_argument(
        "-c", "--pseudo_count",
        type=float,
        default=0,
        help="This gets added to the counts for every reference "
        "in order to avoid dividing by zero.\n"
        "If set to zero (default), some minimum value "
        "will be inferred from the data and used as pseudo-counts.")
    parser.add_argument(
        "-d", "--plot_dir",
        required=True,
        help="Directory in which plots should be written.")
    parser.add_argument(
        "-p", "--plot_name",
        help="Base name for the plot files.")
    parser.add_argument(
        "-e", "--exclude_set",
        help="File contining names of elements to exclude.\n"
        "There should be one element per line.")
    parser.add_argument(
        "--label_size",
        type=int,
        help="Font size for axis labels",
        #default=12)
        default=24)
    parser.add_argument(
        "--hm_label_size",
        type=int,
        help="Font size for fold heatmap labels",
        default=9)
    parser.add_argument(
        "--legend_size",
        type=int,
        help="Font size for legend labels",
        default=9)
    parser.add_argument(
        "--tick_size",
        type=int,
        help="Font size for ticks",
        #default=12)
        default=18)
    #parser.add_argument(
    #    "-x", "--x_axis",
    #    help="Label for the x axis of the plot.",
    #    required=True)
    #parser.add_argument(
    #    "-y", "--y_axis",
    #    help="Label for the y axis of the plot.",
    #    required=True)
    parser.add_argument(
        "-t", "--transform",
        required=True,
        help="log2, log10, or a linear scale to apply.")
    args = parser.parse_args()
    # Not allowing (0, 0) data pairs should make the approach more stringent
    # because it will likely modify more the genes results than the piRNA
    # clusters results, in a way that makes the median ratio more free to go
    # away from 1
    Globals.no_zero_pairs = True
    #########################################
    # Get the list of references to exclude #
    #########################################
    if args.exclude_set:
        with open(args.exclude_set, "r") as elements_file:
            for line in imap(strip, elements_file):
                if line[0] == "#":
                    continue
                Globals.exclude_set.add(line)
    ################
    # Get the data #
    ################
    lib_pairs = []
    for data_file, ref_type in zip(args.in_files, args.ref_types):
        lib1, lib2 = get_data(data_file, ref_type)
        lib_pairs.append((lib1, lib2))
    lib_pairs = set(lib_pairs)
    msg = "There should be only two libraries in the data."
    assert len(lib_pairs) == 1, msg
    # extract library names from the set
    [(lib1, lib2)] = list(lib_pairs)
    if args.pseudo_count:
        Globals.pseudo_count = args.pseudo_count
    else:
        non_zero_mins = []
        for ref_type in args.ref_types:
            [lib1_vals, lib2_vals] = map(
                np.asarray,
                zip(*Globals.pairs[ref_type]))
            min1 = min(lib1_vals[lib1_vals.nonzero()])
            min2 = min(lib2_vals[lib2_vals.nonzero()])
            non_zero_mins.append(min(min1, min2))
        Globals.pseudo_count = min(non_zero_mins)
    #######################
    # Apply_pseudo_counts #
    #######################
    psc = Globals.pseudo_count
    sys.stderr.write("Appliyng %f as pseudocount.\n" % psc)
    for ref_type in args.ref_types:
        pairs = Globals.pairs[ref_type]
        Globals.ratios[ref_type] = np.asarray(
            [(c1 + psc) / (c2 + psc) for (c1, c2) in pairs],
            dtype=np.float)
    #####################
    # Plotting the data #
    #####################
    if args.transform == "log2":
        transform = 2
    elif args.transform == "log10":
        transform = 10
    else:
        transform = False
    # Plot istribution of the folds for the non-focus references
    #axis = plt.gca()
    #histo(axis, lib1, lib2)
    #if args.plot_name:
    #    out_pdf = OPJ(
    #        args.plot_dir,
    #        "%s_vs_%s_histo.pdf" % (lib1, lib2))
    #    plt.savefig(out_pdf)
    # Plot the folds in a boxplot
    #plt.cla()
    axis = plt.gca()
    box_plot(
        axis,
        lib1,
        lib2,
        args.ref_types,
        transform=transform)
    if args.plot_name:
        out_pdf = OPJ(
            args.plot_dir,
            "%s.pdf" % args.plot_name)
        plt.savefig(out_pdf)
    plt.cla()
    if len(args.ref_types) == 2:
        data1 = Globals.ratios[args.ref_types[0]]
        data2 = Globals.ratios[args.ref_types[1]]
        point_name = "%s/%s" % (lib1, lib2)
        point_coords = "%f\t%f" % median_fold_and_pvalue(data1, data2)
        WRITE("%s\t%s\n" % (point_name, point_coords))

if __name__ == "__main__":
    sys.exit(main())
