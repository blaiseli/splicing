We want to compare splicing activity in piRNA clusters and in genes.

We will use data from [Zhang et al (2014)](http://dx.doi.org/10.1016/j.cell.2014.04.030).

    #[[ ${LIBS} ]] || LIBS="w1 rhino_KD Ore_R cn_bw"

Or newly sequenced data.
    
    [[ ${LIBS} ]] || LIBS="shwhite_rep1 shpiwi_rep1 shwhite_rep2 shpiwi_rep2"

Generating bed file for piRNA cluster coordinates
=================================================

    mkdir -p Annotations
    if [[ ! -e Annotations/piRNA_cluster.bed ]]
    then
        grep "piRNA_cluster@" ~/Genomes/D_melanogaster/Annotations/D_melanogaster_flybase_splitmRNA_and_TE_annot.bed \
            | sed -e 's/^/chr/' \
            > Annotations/piRNA_cluster.bed
    fi
    #for i in $(seq 1 142)
    #do
    #    if [[ ! -e Annotations/piRNA_cluster${i}.bed ]]
    #    then
    #        grep "\bpiRNA_cluster@cluster${i}\b" Annotations/piRNA_cluster.bed > Annotations/piRNA_cluster${i}.bed
    #    fi
    #done


Preparing annotations
=====================

    if [[ ! -e Annotations/genes.bed ]]
    then
        # Get transcriptome annotations
        wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/gff/dmel-all-r5.9.gff.gz
        gunzip dmel-all-r5.9.gff.gz
        # Get gene information
        wget --continue ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.9_FB2008_06/fasta/dmel-all-gene-r5.9.fasta.gz
        gunzip dmel-all-gene-r5.9.fasta.gz
        fbfasta2bed.py dmel-all-gene-r5.9.fasta \
            | sort -k1,1 -k2n,2 -k3n,3 \
            > Annotations/genes.bed
    fi

TODO: Faire genes - pirna clusters
TODO: Normaliser par les mappers non rRNA 


Getting the data
================

```
severine.chambeyron@yoshi:~/projet_Abdou/splicing$ l ~/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/
total 23G
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 2.7G Mar 24 22:42 Chambeyron_4_shpiwi_rep2_S4_R2_001.fastq.gz
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 2.7G Mar 24 22:42 Chambeyron_4_shpiwi_rep2_S4_R1_001.fastq.gz
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 3.0G Mar 24 22:42 Chambeyron_3_shwhite_rep2_S3_R2_001.fastq.gz
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 3.0G Mar 24 22:42 Chambeyron_3_shwhite_rep2_S3_R1_001.fastq.gz
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 2.8G Mar 24 22:42 Chambeyron_2_shpiwi_rep1_S2_R2_001.fastq.gz
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 2.8G Mar 24 22:42 Chambeyron_2_shpiwi_rep1_S2_R1_001.fastq.gz
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 3.1G Mar 24 22:42 Chambeyron_1_shwhite_rep1_S1_R2_001.fastq.gz
-rw-r--r-- 1 severine.chambeyron severine.chambeyron 3.1G Mar 24 22:42 Chambeyron_1_shwhite_rep1_S1_R1_001.fastq.gz
```

    raw_data_dir="."
    mkdir -p ${raw_data_dir}
    (
        cd ${raw_data_dir} 
        if [[ ! -e w1_1.fastq.gz || ! -e w1_2.fastq.gz ]]
        then
            fastq-dump -F --split-3 --gzip -A w1 SRR1293157
        fi
        if [[ ! -e rhino_KD_1.fastq.gz || ! -e rhino_KD_2.fastq.gz ]]
        then
            fastq-dump -F --split-3 --gzip -A rhino_KD SRR1024412
        fi
        if [[ ! -e Ore_R_1.fastq.gz || ! -e Ore_R_2.fastq.gz ]]
        then
            fastq-dump -F --split-3 --gzip -A Ore_R SRR1024473
        fi
        if [[ ! -e cn_bw_1.fastq.gz || ! -e cn_bw_2.fastq.gz ]]
        then
            fastq-dump -F --split-3 --gzip -A cn_bw SRR1024342
        fi
        # march 2016 RNA-Seq libraries
        if [[ ! -e shwhite_rep1_1.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_1_shwhite_rep1_S1_R1_001.fastq.gz shwhite_rep1_1.fastq.gz
        fi
        if [[ ! -e shwhite_rep1_2.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_1_shwhite_rep1_S1_R2_001.fastq.gz shwhite_rep1_2.fastq.gz
        fi
        if [[ ! -e shpiwi_rep1_1.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_2_shpiwi_rep1_S2_R1_001.fastq.gz shpiwi_rep1_1.fastq.gz
        fi
        if [[ ! -e shpiwi_rep1_2.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_2_shpiwi_rep1_S2_R2_001.fastq.gz shpiwi_rep1_2.fastq.gz
        fi
        if [[ ! -e shwhite_rep2_1.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_3_shwhite_rep2_S3_R1_001.fastq.gz shwhite_rep2_1.fastq.gz
        fi
        if [[ ! -e shwhite_rep2_2.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_3_shwhite_rep2_S3_R2_001.fastq.gz shwhite_rep2_2.fastq.gz
        fi
        if [[ ! -e shpiwi_rep2_1.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_4_shpiwi_rep2_S4_R1_001.fastq.gz shpiwi_rep2_1.fastq.gz
        fi
        if [[ ! -e shpiwi_rep2_2.fastq.gz ]]
        then
            ln -s ${HOME}/raw_data/Raw_data_Chambeyron/libraries_RNA-Seq_mars_2016/Chambeyron/Chambeyron_4_shpiwi_rep2_S4_R2_001.fastq.gz shpiwi_rep2_2.fastq.gz
        fi
    )


Testing tophat junction detection
=================================


Running tophat
--------------

    genomes_path=${GENOMES}
    genome="D_melanogaster"
    genome_db=${genomes_path}/${genome}/${genome}
    tophat_out_dir="tophat_junction_discovery"
    mkdir -p ${tophat_out_dir}/logs
    for lib in ${LIBS}
    do
        mkdir -p ${tophat_out_dir}/${lib}
        fastq_1="${raw_data_dir}/${lib}_1.fastq.gz"
        fastq_2="${raw_data_dir}/${lib}_2.fastq.gz"
        # We use the options given page 1361 of http://dx.doi.org/10.1016/j.cell.2014.04.030
        if [[ ! -e ${tophat_out_dir}/${lib}/junctions.bed ]]
        then
            #time tophat -p $(nproc) -o ${tophat_out_dir}/${lib} ${genome_db} ${fastq_1} ${fastq_2} \
            time tophat -p 2 -o ${tophat_out_dir}/${lib} ${genome_db} ${fastq_1} ${fastq_2} \
                -G Annotations/dmel-all-r5.9.gff \
                -x 1000 -g 1000 \
                --read-mismatches 2 \
                --read-edit-dist 2 \
                --read-realign-edit-dist 0 \
                --segment-length 50 \
                --segment-mismatches 2 \
                >> ${tophat_out_dir}/logs/${lib}.log \
                2>> ${tophat_out_dir}/logs/${lib}.err
        fi
    done


Generate bed file for genes not overlapping piRNA clusters
----------------------------------------------------------

# Keep only one `chrom_sizes.txt` file

    if [[ ! -e ${tophat_out_dir}/chrom_sizes.txt ]]
    then
        for lib in ${LIBS}
        do
            samtools view -H ${tophat_out_dir}/accepted_hits.bam \
                | awk '$1 == "@SQ" {print $2"\t"$3}' \
                | sed 's/[SL]N://g'
        done | sort -u > ${tophat_out_dir}/chrom_sizes.txt
    fi

# Generate the complement of the piRNA cluster coordinates

    tophat_out_dir="tophat_junction_discovery"
    cluster_coords="Annotations/piRNA_cluster.bed"
    bedtools complement \
        -i ${cluster_coords} \
        -g ${tophat_out_dir}/chrom_sizes.txt \
        > Annotations/not_cluster.bed

# Filter the gene coordinates in order to only keep those not overlapping a piRNA cluster

    bedtools intersect \
        -a Annotations/genes.bed \
        -b Annotations/not_cluster.bed \
        -f 1.0 \
        -u \
        -wa \
        > Annotations/genes_outside_clusters.bed



Counting proportion spliced reads mapping on piRNA clusters and mRNAs
---------------------------------------------------------------------

    #tophat_out_dir="tophat_junction_discovery"
    for lib in ${LIBS}
    do
        # Count junctions in piRNA clusters
        outfile="${tophat_out_dir}/junction_counts/${lib}/piRNA_cluster_junction_counts.txt"
        if [[ ! -e ${outfile} ]]
        then
            junction_file="${tophat_out_dir}/${lib}/junctions.bed"
            echo -e "#library\treference\tjunction_reads_per_kb" > ${outfile}
            cluster_coords=Annotations/piRNA_cluster.bed
            bedtools intersect \
                -a ${cluster_coords} \
                -b ${junction_file} \
                -loj \
                -F 1 \
                | awk -v lib=${lib} '($11 < 1){counts[$4] += 0} ($11 > 0){counts[$4] += (1000 * $11) / ($3 - $2)} END {for (clust in counts) print lib"\t"clust"\t"counts[clust]}' \
                | sed 's/piRNA_cluster@//' \
                | sort -n -k2.9 \
                >> ${outfile}
        fi
    done
    #tophat_out_dir="tophat_junction_discovery"
    for lib in ${LIBS}
    do
        # Count junctions in genes
        outfile="${tophat_out_dir}/junction_counts/${lib}/gene_junction_counts.txt"
        if [[ ! -e ${outfile} ]]
        then
            junction_file="${tophat_out_dir}/${lib}/junctions.bed"
            echo -e "#library\treference\tjunction_reads_per_kb" > ${outfile}
            # Perform a "left outer join".
            # That is, for each feature in A report each overlap with B.
            # If no overlaps are found, report a NULL feature for B.
            # -loj
            # NULL features have -1 in the field corresponding to the junction read counts
            # (field 11 in the resulting output).
            # We want junctions that are fully inside the gene:
            # Minimum overlap required as a fraction of B. Default is 1E-9 (i.e., 1bp).
            # -F 1
            bedtools intersect \
                -a Annotations/genes_outside_clusters.bed \
                -b ${junction_file} \
                -loj \
                -F 1 \
                | awk -v lib=${lib} '($11 < 1){counts[$4] += 0} ($11 > 0){counts[$4] += (1000 * $11) / ($3 - $2)} END {for (gene in counts) print lib"\t"gene"\t"counts[gene]}' \
                >> ${outfile}
        fi
    done


Finding normalization values
----------------------------

# Quick and dirty

    #tophat_out_dir="tophat_junction_discovery"
    for lib in ${LIBS}
    do
        awk '$1 == "Mapped" {sum += $3} END {print sum}' \
            ${tophat_out_dir}/${lib}/align_summary.txt \
            > ${tophat_out_dir}/nb_mappers_${lib}.txt
    done



Snakemake implementation
========================

The above steps have been implemented using snakemake.
They can be executed typing `snakemake -p -s tophat_junction_discovery.snakefile`



Checking that clusters and genes are in the same order in all libraries
-----------------------------------------------------------------------

    echo "The following md5sums should be equal:"
    for lib in ${LIBS}
    do
        awk '{print $2}' ${tophat_out_dir}/junction_counts/${lib}/piRNA_cluster_junction_counts.txt | md5sum
    done

    echo "The following md5sums should be equal:"
    for lib in ${LIBS}
    do
        awk '{print $2}' ${tophat_out_dir}/junction_counts/${lib}/gene_junction_counts.txt | md5sum
    done


Plotting normalized junction read ratios
----------------------------------------

    tophat_out_dir="tophat_junction_discovery"
    counts_dir="${tophat_out_dir}/junction_countss"
    ratio_dir="${tophat_out_dir}/junction_read_ratios"
    mkdir -p ${ratio_dir}/boxplots

# with rhino_KD as "mutant"

    mut="rhino_KD"
    controls="w1 Ore_R cn_bw"
    mut_clust_data="${counts_dir}/${mut}/piRNA_cluster_junction_counts.txt"
    mut_gene_data="${counts_dir}/${mut}/gene_junction_counts.txt"
    for ctrl in ${controls}
    do
        ctrl_clust_data="${counts_dir}/${ctrl}/piRNA_cluster_junction_counts.txt"
        ctrl_gene_data="${counts_dir}/${ctrl}/gene_junction_counts.txt"
        clust_data=${ratio_dir}/piRNA_cluster_junction_counts_${mut}_${ctrl}.txt
        gene_data=${ratio_dir}/gene_junction_counts_${mut}_${ctrl}.txt
        paste ${mut_gene_data} ${ctrl_gene_data} \
            | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
            > ${gene_data}
        paste ${mut_clust_data} ${ctrl_clust_data} \
            | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
            > ${clust_data}
        for psc in 1 2 5 10
        do
            plot_spliced_ratio.py \
                -i ${gene_data} ${clust_data} \
                -c ${psc} -t log2 \
                -l ../listes_refs/cluster_names_1-8_17.txt \
                -d ${ratio_dir}/boxplots -p ${mut}_${ctrl}_${psc}_pseudocounts_boxplot
        done
    done

# with Ore_R as "pseudo mutant"

    mut="Ore_R"
    controls="w1 cn_bw"
    mut_clust_data="${counts_dir}/${mut}/piRNA_cluster_junction_counts.txt"
    mut_gene_data="${counts_dir}/${mut}/gene_junction_counts.txt"
    for ctrl in ${controls}
    do
        ctrl_clust_data="${counts_dir}/${ctrl}/piRNA_cluster_junction_counts.txt"
        ctrl_gene_data="${counts_dir}/${ctrl}/gene_junction_counts.txt"
        clust_data=${ratio_dir}/piRNA_cluster_junction_counts_${mut}_${ctrl}.txt
        gene_data=${ratio_dir}/gene_junction_counts_${mut}_${ctrl}.txt
        paste ${mut_gene_data} ${ctrl_gene_data} \
            | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
            > ${gene_data}
        paste ${mut_clust_data} ${ctrl_clust_data} \
            | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
            > ${clust_data}
        for psc in 1 2 5 10
        do
            plot_spliced_ratio.py \
                -i ${gene_data} ${clust_data} \
                -c ${psc} -t log2 \
                -l ../listes_refs/cluster_names_1-8_17.txt \
                -d ${ratio_dir}/boxplots -p ${mut}_${ctrl}_${psc}_pseudocounts_boxplot
        done
    done

# with cn_bw as "pseudo mutant"

    mut="cn_bw"
    controls="w1"
    mut_clust_data="${counts_dir}/${mut}/piRNA_cluster_junction_counts.txt"
    mut_gene_data="${counts_dir}/${mut}/gene_junction_counts.txt"
    for ctrl in ${controls}
    do
        ctrl_clust_data="${counts_dir}/${ctrl}/piRNA_cluster_junction_counts.txt"
        ctrl_gene_data="${counts_dir}/${ctrl}/gene_junction_counts.txt"
        clust_data=${ratio_dir}/piRNA_cluster_junction_counts_${mut}_${ctrl}.txt
        gene_data=${ratio_dir}/gene_junction_counts_${mut}_${ctrl}.txt
        paste ${mut_gene_data} ${ctrl_gene_data} \
            | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
            > ${gene_data}
        paste ${mut_clust_data} ${ctrl_clust_data} \
            | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
            > ${clust_data}
        for psc in 1 2 5 10
        do
            plot_spliced_ratio.py \
                -i ${gene_data} ${clust_data} \
                -c ${psc} -t log2 \
                -l ../listes_refs/cluster_names_1-8_17.txt \
                -d ${ratio_dir}/boxplots -p ${mut}_${ctrl}_${psc}_pseudocounts_boxplot
        done
    done


    for m in 0 0.0000000001 0.0000001 0.0001; do plot_splicing_intensity.py -i tophat_junction_discovery/junction_counts/rhino_KD/gene_junction_counts.txt tophat_junction_discovery/junction_counts/w1/gene_junction_counts.txt tophat_junction_discovery/junction_counts/Ore_R/gene_junction_counts.txt tophat_junction_discovery/junction_counts/cn_bw/gene_junction_counts.txt -l rhino_KD w1 Ore_R cn_bw -d tophat_junction_discovery/junction_counts/boxplots -p genes_min_${m} -t log10 -m ${m} -n 89223146 99698688 88327542 67740959; done
    for m in 0 0.0000000001 0.0000001 0.0001; do plot_splicing_intensity.py -i tophat_junction_discovery/junction_counts/rhino_KD/piRNA_cluster_junction_counts.txt tophat_junction_discovery/junction_counts/w1/piRNA_cluster_junction_counts.txt tophat_junction_discovery/junction_counts/Ore_R/piRNA_cluster_junction_counts.txt tophat_junction_discovery/junction_counts/cn_bw/piRNA_cluster_junction_counts.txt -l rhino_KD w1 Ore_R cn_bw -d tophat_junction_discovery/junction_counts/boxplots -p piRNA_clusters_min_${m} -t log10 -m ${m} -n 89223146 99698688 88327542 67740959; done

TODO: Make better normalization
TODO: Work with mut/ctrl folds, find in which percentile each piRNA cluster is situated with respect to genes
