We want to compare splicing activity in piRNA clusters and in genes.

We will use data from [Zhang et al (2014)](http://dx.doi.org/10.1016/j.cell.2014.04.030).

    #[[ ${LIBS} ]] || LIBS="w1 rhino_KD Ore_R cn_bw"

Or newly sequenced data.
    
    #[[ ${LIBS} ]] || LIBS="shwhite_rep1 shpiwi_rep1 shwhite_rep2 shpiwi_rep2"


Previous steps
==============

Previous to figure generation, tophat has to be run and read junction counts have to be extracted.
This can be run typing `snakemake -p -s tophat_junction_discovery.snakefile`



Checking that clusters and genes are in the same order in all libraries
-----------------------------------------------------------------------

    echo "The following md5sums should be equal:"
    for lib in ${LIBS}
    do
        awk '{print $2}' ${tophat_out_dir}/junction_counts/${lib}/piRNA_cluster_junction_counts.txt | md5sum
    done

    echo "The following md5sums should be equal:"
    for lib in ${LIBS}
    do
        awk '{print $2}' ${tophat_out_dir}/junction_counts/${lib}/gene_junction_counts.txt | md5sum
    done


Plotting normalized junction read ratios
----------------------------------------


    #LIBS="shpiwi_rep1 shwhite_rep1 shpiwi_rep2 shwhite_rep2 rhino_KD w1 Ore_R cn_bw"
    LIBS_Zhang="rhino_KD w1 Ore_R cn_bw"
    LIBS_Akkouche="shwhite_rep1 shwhite_rep2 shpiwi_rep1 shpiwi_rep2"
    LIBS=${LIBS_Akkouche}
    tophat_out_dir="tophat_junction_discovery"
    # Build parameter lists
    norms=""
    cluster_gene_inputs=""
    gene_inputs=""
    piRNA_cluster_inputs=""
    for lib in ${LIBS}
    do
        norm=$(awk '$1 == "Mapped" {sum += $3} END {printf "%f\n", 1000000/sum}' \
            ${tophat_out_dir}/${lib}/align_summary.txt)
        norms="${norms} ${norm}"
        cluster_gene_input="${tophat_out_dir}/junction_counts/${lib}/cluster_gene_junction_counts.txt"
        cluster_gene_inputs="${cluster_gene_inputs} ${cluster_gene_input}"
        gene_input="${tophat_out_dir}/junction_counts/${lib}/gene_junction_counts.txt"
        gene_inputs="${gene_inputs} ${gene_input}"
        piRNA_cluster_input="${tophat_out_dir}/junction_counts/${lib}/piRNA_cluster_junction_counts.txt"
        piRNA_cluster_inputs="${piRNA_cluster_inputs} ${piRNA_cluster_input}"
    done
    
    log_dir="${tophat_out_dir}/junction_counts/boxplots"
    mkdir -p ${log_dir}
    thresholds="0 0.0000000001"
    exclusion_list="${tophat_out_dir}/junction_counts/boxplots/exclusion_list.txt"
    echo -e "cluster8\ncluster17" > ${exclusion_list}
    for m in ${thresholds}
    do
        plot_name="cluster_genes_min_${m}"
        plot_splicing_intensity.py \
            -i ${cluster_gene_inputs} \
            -l ${LIBS} \
            -d ${tophat_out_dir}/junction_counts/boxplots \
            -p cluster_genes_min_${m} \
            -p ${plot_name} \
            -t log10 \
            -m ${m} \
            -n ${norms} \
            -y "RPKM for cluster genes" \
            1> ${log_dir}/${plot_name}.log \
            2> ${log_dir}/${plot_name}.log
        plot_name="genes_min_${m}"
        plot_splicing_intensity.py \
            -i ${gene_inputs} \
            -l ${LIBS} \
            -d ${tophat_out_dir}/junction_counts/boxplots \
            -p genes_min_${m} \
            -p ${plot_name} \
            -t log10 \
            -m ${m} \
            -n ${norms} \
            -y "RPKM for genes" \
            1> ${log_dir}/${plot_name}.log \
            2> ${log_dir}/${plot_name}.log
        plot_name="piRNA_clusters_min_${m}"
        plot_splicing_intensity.py \
            -i ${piRNA_cluster_inputs} \
            -l ${LIBS} \
            -d ${tophat_out_dir}/junction_counts/boxplots \
            -p piRNA_clusters_min_${m} \
            -p ${plot_name} \
            -t log10 \
            -m ${m} \
            -n ${norms} \
            -y "RPKM for piRNA clusters" \
            1> ${log_dir}/${plot_name}.log \
            2> ${log_dir}/${plot_name}.log
        plot_name="piRNA_clusters_no_8_17_min_${m}"
        plot_splicing_intensity.py \
            -i ${piRNA_cluster_inputs} \
            -l ${LIBS} \
            -d ${tophat_out_dir}/junction_counts/boxplots \
            -p ${plot_name} \
            -t log10 \
            -m ${m} \
            -e ${exclusion_list} \
            -n ${norms} \
            -y "RPKM for piRNA clusters (no_8_17)" \
            1> ${log_dir}/${plot_name}.log \
            2> ${log_dir}/${plot_name}.log
    done

TODO: Make better normalization
TODO: Work with mut/ctrl folds, find in which percentile each piRNA cluster is situated with respect to genes


Plotting ratios junction read counts per kb
-------------------------------------------

    tophat_out_dir="tophat_junction_discovery"
    counts_dir="${tophat_out_dir}/junction_counts"
    volcano_dir="${counts_dir}/volcano_plots"
    mkdir -p ${volcano_dir}

    #echo -e "shwhite_rep1\nshwhite_rep2\nw1\nOre_R\ncn_bw" > controls.txt
    #echo -e "shpiwi_rep1\nshpiwi_rep2\nrhino_KD" > mutants.txt 
    #echo -e "shwhite_rep1\nshwhite_rep2\nw1\nOre_R" > controls.txt
    #echo -e "rhino_KD" > mutants_Zhang.txt 
    #echo -e "shpiwi_rep1\nshpiwi_rep2" > mutants_Akkouche.txt 
    controls="shwhite_rep1 shwhite_rep2 w1 Ore_R"
    mutants_Zhang="rhino_KD"
    mutants_Akkouche="shpiwi_rep1 shpiwi_rep2"
    mutants="${mutants_Zhang} ${mutants_Akkouche}"

    psc="0.001"
    volcano_file="${volcano_dir}/ratio_${psc}_pseudocounts_volcano.txt"
    rm -f ${volcano_file}
    touch ${volcano_file}
    # Using normal "mutants"
    for mut in ${mutants}
    do
        for ctrl in ${controls}
        do
            compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}
        done
    done

    # Using controls as "mutants"

    ctrl="Ore_R"
    mut="shwhite_rep2"
    compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}
    
    mut="Ore_R"
    ctrl="shwhite_rep1"
    compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}
    
    #ctrl="shwhite_rep2"
    #compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}

    ctrl="w1"
    compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}

    mut="w1"
    ctrl="shwhite_rep1"
    compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}
    
    ctrl="shwhite_rep2"
    compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}

    mut="shwhite_rep2"
    ctrl="shwhite_rep1"
    compute_ratios_and_make_boxplot.sh ${mut} ${ctrl} ${counts_dir} ${psc} ${volcano_file}

    plot_ratio_volcano.py -i ${volcano_file} \
        -d ${volcano_dir} \
        -p ratio_${psc}_pseudocounts_volcano \
        -c colours.txt
