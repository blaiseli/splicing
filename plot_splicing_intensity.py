#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script will read splicing quantification and plot the information."""


import argparse
import sys
WRITE = sys.stdout.write

# To avoid repeatedly calling these as string methods:
from string import split, strip
#from re import sub

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

#from itertools import cycle, imap, izip, repeat
from itertools import combinations, imap, repeat
#from collections import OrderedDict
from collections import defaultdict
#from operator import itemgetter
from scipy import stats
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
mpl.use("PDF")

#mpl.rcParams["figure.figsize"] = 2, 4
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"

#import pylab
from matplotlib import numpy as np
import matplotlib.pyplot as plt


def get_data(data_file_name, min_val=0):
    """
    Reads data from file *data_file_name* where the file contains three
    tab-separated columns.

    The first line is a header line, as follows:
    #library        reference       junction_reads_per_kb

    The second columns contains the name a a feature (piRNA cluster, gene...).

    The third column contains the number of the junction read counts normalized
    by the length of the feature.

    The normalized counts are recorded in Globals.counts
    if they are at least equal to *min_val*.
    """
    counts = Globals.counts
    exclude_set = Globals.exclude_set
    with open(data_file_name, "r") as data_file:
        #[lib, _, _] = split(strip(data_file.readline().lstrip("#")), "\t")

        def extract_fields(line):
            """Selects and appropriately converts the fields we want."""
            [lib, ref, count] = split(line)
            return lib, ref, float(count)
        for line in imap(strip, data_file):
            if line[0] == "#":
                continue
            lib, ref, count = extract_fields(line)
            if count >= min_val and ref not in exclude_set:
                assert ref not in counts[lib]
                counts[lib][ref] = count


#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################


class Globals(object):
    """This object holds global variables."""
    # Set of elements to exclude from the boxplot data
    exclude_set = set()
    # key: library name
    # value: {reference : normalized count}
    counts = defaultdict(dict)


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-i", "--in_files",
        nargs="*",
        required=True,
        help="Space-separated list of data files.")
    parser.add_argument(
        "-l", "--lib_order",
        nargs="*",
        help="Space-separated list of library names.\n"
        "These should correspond to names found in first columns "
        "of data files")
    parser.add_argument(
        "-n", "--norm_values",
        type=float,
        nargs="*",
        help="Space-separated list of normalization factors to apply.\n"
        "These should be in the same order as the libraries indicated "
        "with option --lib_order.")
    parser.add_argument(
        "-d", "--plot_dir",
        required=True,
        help="Directory in which plots should be written.")
    parser.add_argument(
        "-p", "--plot_name",
        help="Base name for the plot files.")
    parser.add_argument(
        "-e", "--exclude_set",
        help="File contining names of elements to exclude.\n"
        "There should be one element per line.")
    parser.add_argument(
        "-m", "--min_val",
        type=float,
        help="Minimum value for a normalized counts to be recorded.",
        default=0)
    parser.add_argument(
        "--label_size",
        type=int,
        help="Font size for axis labels",
        #default=12)
        default=24)
    parser.add_argument(
        "--hm_label_size",
        type=int,
        help="Font size for fold heatmap labels",
        default=9)
    parser.add_argument(
        "--legend_size",
        type=int,
        help="Font size for legend labels",
        default=9)
    parser.add_argument(
        "--tick_size",
        type=int,
        help="Font size for ticks",
        #default=12)
        default=18)
    #parser.add_argument(
    #    "-x", "--x_axis",
    #    help="Label for the x axis of the plot.",
    #    required=True)
    parser.add_argument(
        "-y", "--y_axis",
        help="Label for the y axis of the plot.",
        default="normalized read counts")
    parser.add_argument(
        "-t", "--transform",
        required=True,
        help="log2, log10, or a linear scale to apply.")
    args = parser.parse_args()
    if args.norm_values:
        msg = """If no library order is provided, it is impossible to know
        which normalization value applies to which library."""
        assert args.lib_order, msg
        msg = """There should be the same number of normalization values
        and libraries."""
        assert len(args.lib_order) == len(args.norm_values), msg
        lib2norm = dict(zip(args.lib_order, args.norm_values))
    else:
        lib2norm = None
    #########################################
    # Get the list of references to exclude #
    #########################################
    if args.exclude_set:
        with open(args.exclude_set, "r") as elements_file:
            for line in imap(strip, elements_file):
                if line[0] == "#":
                    continue
                Globals.exclude_set.add(line)
    ################
    # Get the data #
    ################
    for data_file in args.in_files:
        get_data(data_file, min_val=args.min_val)
    if args.lib_order:
        lib_order = args.lib_order
    else:
        lib_order = sorted(Globals.counts.keys())
    if lib2norm is None:
        lib2norm = dict(zip(lib_order, repeat(1.0)))
    #####################
    # Plotting the data #
    #####################
    if args.transform == "log2":
        transform = 2
    elif args.transform == "log10":
        transform = 10
    else:
        transform = False
    #references = set()
    #for counts_dict in Globals.counts.values():
    #    for ref in counts_dict:
    #        references.add(ref)
    #references = sorted(references)
    #references = sorted(set(
    #    (ref for ref in counts_dict for counts_dict in Globals.counts.values())))

    #ordered_by_ref = {}
    #for lib in lib_order:
    #    ordered_by_ref[lib] = np.asarray(
    #        [Globals.counts[lib][ref] for ref in references])
    normed_data = {}
    for lib in lib_order:
        normed_data[lib] = np.asarray(
            Globals.counts[lib].values(),
            dtype="float") / lib2norm[lib]
    WRITE("Wilcoxon rank-sum test p-values for %s:\n" % args.plot_name)
    WRITE("===============================\n")
    WRITE("Low p-values mean that the distributions ")
    WRITE("are significantly different.\n")
    WRITE("Remember that low p-values can be obtained ")
    WRITE("when there are a lot of data points even if the ")
    WRITE("difference between distributions is quantitatively small.\n")
    for lib1, lib2 in combinations(lib_order, 2):
        data_1 = normed_data[lib1]
        data_2 = normed_data[lib2]
        #data_1 = ordered_by_ref[lib1]
        #data_2 = ordered_by_ref[lib2]
        _, p_value = stats.ranksums(data_1, data_2)
        if p_value < 0.01:
            WRITE("%s vs %s: %f **\n" % (lib1, lib2, p_value))
        elif p_value < 0.05:
            WRITE("%s vs %s: %f *\n" % (lib1, lib2, p_value))
        else:
            WRITE("%s vs %s: %f\n" % (lib1, lib2, p_value))
    #WRITE("Mann-Whitney rank test p-values:\n")
    #for lib1, lib2 in combinations(lib_order, 2):
    #    data_1 = ordered_by_ref[lib1]
    #    data_2 = ordered_by_ref[lib2]
    #    _, p_value = stats.mannwhitneyu(data_1, data_2)
    #    WRITE("%s vs %s: %f\n" % (lib1, lib2, p_value))

    box_data = [normed_data[lib] for lib in lib_order]
    #box_data = [np.asarray(
    #    [Globals.counts[lib][ref] for ref in references]) for lib in lib_order]
    axis = plt.gca()
    if transform:
        axis.set_yscale("log", basey=transform)
        if transform == 2:
            plt.minorticks_off()
    axis.boxplot(box_data, whis=[5, 95])
    x_labels = lib_order
    #plt.xticks(rotation=70)
    axis.set_ylabel(args.y_axis)
    axis.set_xticklabels(x_labels)
    if args.plot_name:
        out_pdf = OPJ(
            args.plot_dir,
            "%s.pdf" % args.plot_name)
        plt.savefig(out_pdf)
    plt.cla()

if __name__ == "__main__":
    sys.exit(main())
