#!/usr/bin/env python
# vim: set fileencoding=<utf-8> :
"""This script reads data from a three-column tab-separated file.
Each line corresponds to a point in a volcano plot.
First column is the point name.
Second column is the x-axis value.
Third column is the p-value.
The p-values are plotted on the y axis, with a "symlog" scale:
The scale is linear below 5*10^(-6) in order to avoid loosing points
with a p-value that has been rounded to 0.
"""


import argparse
import sys
WRITE = sys.stdout.write

# To avoid repeatedly calling these as string methods:
from string import split, strip
#from re import sub

import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)
warnings.formatwarning = formatwarning

import os
OPJ = os.path.join
OPB = os.path.basename

from collections import namedtuple
import matplotlib as mpl
# To be able to run the script without a defined $DISPLAY
mpl.use("PDF")

#mpl.rcParams["figure.figsize"] = 2, 4
mpl.rcParams["font.sans-serif"] = [
    "Arial", "Liberation Sans", "Bitstream Vera Sans"]
mpl.rcParams["font.family"] = "sans-serif"

from math import log
#import pylab
#from matplotlib import numpy as np
import matplotlib.pyplot as plt


# Volcano plot point
Vpoint = namedtuple("Vpoint", ["label", "median_fold", "p_value", "colour"])

#class Vpoint(object):
#    """Volcano plot point"""
#    __slots__ = ("label", "median_fold", "p_value")


class Series(dict):
    """Represents a series of points."""

    def __init__(self, colour):
        super(Series, self).__init__()
        self.colour = colour


class Scatterplot(object):
    """A 2-dimension scatterplot."""

    def __init__(self, colours_file_name):
        self.colours = {}
        self.get_colours(colours_file_name)
        self.data = {}
        #self.label_offsets = {}

    #def libs2series(self, lib1, lib2):
    #    """Returns the appropriate Series."""
    #    pair = "/".join([lib1, lib2])
    #    if pair in self.data:
    #        return self.data[pair]
    #    else:
    #        return self.data("/".join([lib2, lib1]))

    def get_colours(self, colours_file_name):
        """Reads association between library pairs and point colours."""
        with open(colours_file_name) as colours_file:
            for line in map(strip, colours_file):
                if line[0] == "#":
                    continue
                (pair, colour) = split(line)
                self.colours[pair] = colour

    def get_data(self, data_file_name):
        """Reads data from file *data_file_name*."""
        with open(data_file_name, "r") as data_file:
            for line in map(strip, data_file):
                if line[0] == "#":
                    continue
                fields = split(line)
                fields[1] = float(fields[1])
                fields[2] = float(fields[2])
                pair = fields[0]
                self.data[pair] = Vpoint(*fields, colour=self.colours[pair])

    #def get_offsets(self, offset_file_name):
    #    """Reads label offsets from file *offset_file_name*"""
    #    with open(offset_file_name, "r") as data_file:
    #        for line in map(strip, data_file):
    #            if line[0] == "#":
    #                continue
    #            lib_pair, x_off, y_off = split(line)
    #            self.label_offsets[lib_pair] = (float(x_off), float(y_off))

    def plot(self,
             x_axis_label,
             y_axis_label,
             #to_annotate=set(["mut/ctrl"]),
             output_files=None,
             #transform=False,
             marker_size=4):
        """Plots the scatterplot into some graphic files."""
        # https://www.quantifiedcode.com/app/issue_class/3P0qV6OB
        output_files = output_files or [os.devnull]
        axis = plt.gca()
        #if transform:
        #    #axis.set_xscale("log", basex=transform)
        #    # "symlog" in order to avoid loosing points with p-value = 0
        #    #axis.set_yscale("symlog", basey=transform, linthreshy=0.000005)
        #    axis.set_yscale("log", basey=transform)
        #    if transform == 2:
        #        plt.minorticks_off()
        #        #axis.set_xscale("log", basex=transform, subsx=[3])
        #        #axis.set_yscale("log", basey=transform)
        #axis.set_autoscale_on(False)
        #axis.set_aspect('equal')
        #axis.set_xmargin(0.5)
        #axis.set_ymargin(0.5)
        min_pvalue = 0.000001
        plt.ylim((0, -log(0.5 * min_pvalue, 10)))
        for point in self.data.values():
            axis.plot(
                point.median_fold,
                -log(max(point.p_value, min_pvalue), 10),
                #point.p_value,
                "o",
                color=point.colour,
                markersize=marker_size)
            #if pair_type in to_annotate:
            #    offsets = self.label_offsets.get(point.label, (20, 20))
            #    axis.annotate(
            #        point.label,
            #        xy=(point.median_fold, point.p_value),
            #        xytext=offsets,
            #        textcoords="offset points",
            #        ha="center",
            #        va="bottom",
            #        arrowprops=dict(
            #            arrowstyle='->',
            #            #connectionstyle='arc3,rad=0.5',
            #            color=colour,
            #            alpha=0.5))
        axis.set_xlabel(x_axis_label)
        axis.set_ylabel(y_axis_label)
        #axis.tick_params(
        #    which="major",
        #    direction="in",
        #    length=9,
        #    width=1)
        #axis.tick_params(
        #    which="minor",
        #    direction="in",
        #    length=4,
        #    width=1)
        axis.tick_params(
            which="major",
            direction="out",
            top=False,
            right=False,
            length=4,
            width=1)
        axis.tick_params(
            which="minor",
            direction="out",
            top=False,
            right=False,
            length=2,
            width=0.5)
        axis.spines["top"].set_visible(False)
        axis.spines["right"].set_visible(False)
        #plt.axis("tight")
        #plt.axis("equal")
        #plt.axis("scaled")
        plt.tight_layout()
        #plt.legend(loc="lower right")
        for out in output_files:
            plt.savefig(out)
        # To avoid successive plots from superimposing on one another
        plt.cla()


#########################################################
# For better traceability, options and global variables #
# should be stored as class attributes                  #
#########################################################


class Globals(object):
    """This object holds global variables."""


def main():
    """Main function of the program."""
    WRITE("%s\n" % " ".join(sys.argv))
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-i", "--in_file",
        required=True,
        help="Input data consisting in lines of tab-separated fields.\n"
        "First column should contain the pair of libraries whose fold "
        "was used to generate the data, joind by a slash.\n"
        "Column 2 should contain median ratios.\n"
        "Column 3 should contain p-values.")
    parser.add_argument(
        "-d", "--plot_dir",
        required=True,
        help="Directory in which the volcano plot should be written.")
    parser.add_argument(
        "-p", "--plot_name",
        help="Base name for the plot file.")
    #parser.add_argument(
    #    "-m", "--mutants",
    #    help="A file contining the names of libraries names "
    #    "that should be considered 'mutants'. One per line.")
    #parser.add_argument(
    #    "-c", "--controls",
    #    help="A file contining the names of libraries names "
    #    "that should be considered 'controls'. One per line.")
    parser.add_argument(
        "-c", "--colours",
        help="A file with tab-separated fields.\n"
        "Column 1 sould contain a pair of libraries, as in --in_file.\n"
        "Column 2 should contain the colour for the corresponding point.")
    #parser.add_argument(
    #    "-o", "--offsets",
    #    help="A file containing label offsets.\n"
    #    "Same structure as --in_file.")
    parser.add_argument(
        "--label_size",
        type=int,
        help="Font size for axis labels",
        #default=12)
        default=24)
    parser.add_argument(
        "--legend_size",
        type=int,
        help="Font size for legend labels",
        default=9)
        #default=18)
    parser.add_argument(
        "--marker_size",
        type=int,
        help="Size for markers on the plot",
        #default=12)
        default=4)
    parser.add_argument(
        "--tick_size",
        type=int,
        help="Font size for ticks",
        #default=12)
        default=18)
    parser.add_argument(
        "-x", "--x_axis",
        help="Label for the x axis of the plot.",
        default="SEV median ratio")
    parser.add_argument(
        "-y", "--y_axis",
        help="Label for the y axis of the plot.",
        default="-log10(p-value)")
    #parser.add_argument(
    #    "-t", "--transform",
    #    required=True,
    #    help="log2, log10, or a linear scale to apply.")
    args = parser.parse_args()
    ####################
    # Getting the data #
    ####################
    scatterplot = Scatterplot(args.colours)
    scatterplot.get_data(args.in_file)
    #if args.offsets:
    #    scatterplot.get_offsets(args.offsets)
    #if args.transform == "log2":
    #    transform = 2
    #elif args.transform == "log10":
    #    transform = 10
    #else:
    #    transform = False
    if args.plot_name:
        out_pdf = OPJ(
            args.plot_dir,
            "%s.pdf" % args.plot_name)
        scatterplot.plot(
            x_axis_label=args.x_axis,
            y_axis_label=args.y_axis,
            output_files=[out_pdf],
            #transform=transform,
            marker_size=args.marker_size)

if __name__ == "__main__":
    sys.exit(main())
