#!/bin/bash
# Usage: compute_ratios_and_make_boxplot.sh <mutant> <control> <counts_dir> <pseudo_count> <volcano_file>

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}



mut="${1}"
ctrl="${2}"
counts_dir="${3}"
if [[ ${4} ]]
then
    psc="${4}"
else
    psc="0"
fi

volcano_file="${5}"

ratio_dir="${counts_dir}/junction_read_ratios"
mkdir -p ${ratio_dir}/boxplots

mut_clust_data="${counts_dir}/${mut}/piRNA_cluster_junction_counts.txt"
mut_gene_data="${counts_dir}/${mut}/gene_junction_counts.txt"
mut_clust_gene_data="${counts_dir}/${mut}/cluster_gene_junction_counts.txt"
ctrl_clust_data="${counts_dir}/${ctrl}/piRNA_cluster_junction_counts.txt"
ctrl_gene_data="${counts_dir}/${ctrl}/gene_junction_counts.txt"
ctrl_clust_gene_data="${counts_dir}/${ctrl}/cluster_gene_junction_counts.txt"
clust_data=${ratio_dir}/piRNA_cluster_junction_counts_${mut}_${ctrl}.txt
gene_data=${ratio_dir}/gene_junction_counts_${mut}_${ctrl}.txt
clust_gene_data=${ratio_dir}/cluster_gene_junction_counts_${mut}_${ctrl}.txt

paste ${mut_clust_gene_data} ${ctrl_clust_gene_data} \
    | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
    > ${clust_gene_data} || error_exit "Paste failed. Missing file ?"
paste ${mut_gene_data} ${ctrl_gene_data} \
    | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
    > ${gene_data} || error_exit "Paste failed. Missing file ?"
paste ${mut_clust_data} ${ctrl_clust_data} \
    | awk -v mut=${mut} -v ctrl=${ctrl} 'BEGIN {print "#reference\t"mut"\t"ctrl} NR > 1 {print $2"\t"$3"\t"$6}' \
    > ${clust_data} || error_exit "Paste failed. Missing file ?"
#plot_spliced_ratio.py \
#    -i ${gene_data} ${clust_data} ${clust_gene_data} \
#    -r genes piRNA_clusters cluster_genes \
#    -c ${psc} -t log2 \
#    -d ${ratio_dir}/boxplots \
#    -p ${mut}_${ctrl}_${psc}_pseudocounts_boxplot || error_exit "Plot failed."

plot_spliced_ratio.py \
    -i ${clust_data} ${gene_data} \
    -r piRNA_clusters genes \
    -c ${psc} -t log2 \
    -d ${ratio_dir}/boxplots \
    -p ${mut}_${ctrl}_${psc}_pseudocounts_boxplot \
    >> ${volcano_file} \
    || error_exit "Plot failed."

boxplot_pdf="${ratio_dir}/boxplots/${mut}_${ctrl}_${psc}_pseudocounts_boxplot.pdf"

echo "ratios ${mut} / ${ctrl} for genes plotted in ${boxplot_pdf}"

exit 0
